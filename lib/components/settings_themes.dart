import 'package:flutter/material.dart';

import '../components/theme_switcher.dart';
import '../components/matrix.dart';
import '../i18n/i18n.dart';

class ThemesSettings extends StatefulWidget {
  @override
  ThemesSettingsState createState() => ThemesSettingsState();
}

class ThemesSettingsState extends State<ThemesSettings> {
  Themes _selectedTheme;
  bool _amoledEnabled;
  bool _qtBackground = false;

  void load() async {
    final MatrixState matrix = Matrix.of(context);
    bool _qBackground = (await matrix.client.storeAPI.getItem("qtbackground")) == "true" ? true : false;
    setState(() {
      _qtBackground = _qBackground;
    });
    
  }

  @override
  Widget build(BuildContext context) {
    final MatrixState matrix = Matrix.of(context);
    final ThemeSwitcherWidgetState themeEngine =
        ThemeSwitcherWidget.of(context);
    _selectedTheme = themeEngine.selectedTheme;
    _amoledEnabled = themeEngine.amoledEnabled;
    load();

    return Column(
      children: <Widget>[
        RadioListTile<Themes>(
          title: Text(
            "H'nyro",
          ),
          value: Themes.light,
          groupValue: _selectedTheme,
          activeColor: Theme.of(context).primaryColor,
          onChanged: (Themes value) {
            setState(() {
              _selectedTheme = value;
              themeEngine.switchTheme(matrix, value, _amoledEnabled);
            });
          },
        ),
        RadioListTile<Themes>(
          title: Text(
            "What",
          ),
          value: Themes.dark,
          groupValue: _selectedTheme,
          activeColor: Theme.of(context).primaryColor,
          onChanged: (Themes value) {
            setState(() {
              _selectedTheme = value;
              themeEngine.switchTheme(matrix, value, _amoledEnabled);
            });
          },
        ),
        ListTile(
          title: Text(
            I18n.of(context).useAmoledTheme,
          ),
          trailing: Switch(
            value: _amoledEnabled,
            activeColor: Theme.of(context).primaryColor,
            onChanged: (bool value) {
              setState(() {
                _amoledEnabled = value;
                themeEngine.switchTheme(matrix, _selectedTheme, value);
              });
            },
          ),
        ),
        ListTile(
            title: Text(
              "Use cute backgrounds",
            ),
            trailing: Switch(
              value: _qtBackground,
              activeColor: Theme.of(context).primaryColor,
              onChanged: (bool value) async {
                await matrix.client.storeAPI.setItem("qtbackground", value.toString());
                setState(() {
                  _qtBackground = value;
                });
              },
            ),
          ),
      ],
    );
  }
}
