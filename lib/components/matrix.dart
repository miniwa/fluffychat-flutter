import 'dart:async';
import 'dart:io';

import 'package:famedlysdk/famedlysdk.dart';
import 'package:fluffychat/components/dialogs/simple_dialogs.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:path_provider/path_provider.dart';
import 'package:toast/toast.dart';

import '../i18n/i18n.dart';
import '../utils/beautify_string_extension.dart';
import '../utils/famedlysdk_store.dart';

class Matrix extends StatefulWidget {
  final Widget child;

  final String clientName;

  final Client client;

  Matrix({this.child, this.clientName, this.client, Key key}) : super(key: key);

  @override
  MatrixState createState() => MatrixState();

  /// Returns the (nearest) Client instance of your application.
  static MatrixState of(BuildContext context) {
    MatrixState newState =
        (context.dependOnInheritedWidgetOfExactType<_InheritedMatrix>()).data;
    newState.context = context;
    return newState;
  }
}

class MatrixState extends State<Matrix> {
  Client client;
  BuildContext context;

  //FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  Map<String, dynamic> shareContent;

  String activeRoomId;

  void clean() async {
    if (!kIsWeb) return;

    final LocalStorage storage = LocalStorage('LocalStorage');
    await storage.ready;
    await storage.deleteItem(widget.clientName);
  }

  BuildContext _loadingDialogContext;

  Future<dynamic> tryRequestWithLoadingDialog(Future<dynamic> request,
      {Function(MatrixException) onAdditionalAuth}) async {
    showLoadingDialog(context);
    final dynamic = await tryRequestWithErrorToast(request,
        onAdditionalAuth: onAdditionalAuth);
    hideLoadingDialog();
    return dynamic;
  }

  Future<dynamic> tryRequestWithErrorToast(Future<dynamic> request,
      {Function(MatrixException) onAdditionalAuth}) async {
    try {
      return await request;
    } on MatrixException catch (exception) {
      if (exception.requireAdditionalAuthentication &&
          onAdditionalAuth != null) {
        return await tryRequestWithErrorToast(onAdditionalAuth(exception));
      } else {
        Toast.show(
          exception.errorMessage,
          context,
          duration: Toast.LENGTH_LONG,
        );
      }
    } catch (exception) {
      Toast.show(
        exception.toString(),
        context,
        duration: Toast.LENGTH_LONG,
      );
      return false;
    }
  }

  showLoadingDialog(BuildContext context) {
    _loadingDialogContext = context;
    showDialog(
      context: _loadingDialogContext,
      barrierDismissible: false,
      builder: (BuildContext context) => AlertDialog(
        content: Row(
          children: <Widget>[
            CircularProgressIndicator(),
            SizedBox(width: 16),
            Text(I18n.of(context).loadingPleaseWait),
          ],
        ),
      ),
    );
  }

  hideLoadingDialog() => Navigator.of(_loadingDialogContext)?.pop();

  Future<String> downloadAndSaveContent(MxContent content,
      {int width, int height, ThumbnailMethod method}) async {
    final bool thumbnail = width == null && height == null ? false : true;
    final String tempDirectory = (await getTemporaryDirectory()).path;
    final String prefix = thumbnail ? "thumbnail" : "";
    File file = File('$tempDirectory/${prefix}_${content.mxc.split("/").last}');

    if (!file.existsSync()) {
      final url = thumbnail
          ? content.getThumbnail(client,
              width: width, height: height, method: method)
          : content.getDownloadLink(client);
      var request = await HttpClient().getUrl(Uri.parse(url));
      var response = await request.close();
      var bytes = await consolidateHttpClientResponseBytes(response);
      await file.writeAsBytes(bytes);
    }

    return file.path;
  }

  void _initWithStore() async {
    Future<LoginState> initLoginState = client.onLoginStateChanged.stream.first;
    client.storeAPI = kIsWeb ? Store(client) : ExtendedStore(client);
    debugPrint(
        "[Store] Store is extended: ${client.storeAPI.extended.toString()}");
    if (await initLoginState == LoginState.logged && !kIsWeb) {
      //await setupFirebase();
    }
  }

  Map<String, dynamic> getAuthByPassword(String password, String session) => {
        "type": "m.login.password",
        "identifier": {
          "type": "m.id.user",
          "user": client.userID,
        },
        "user": client.userID,
        "password": password,
        "session": session,
      };

  StreamSubscription onRoomKeyRequestSub;

  @override
  void initState() {
    if (widget.client == null) {
      debugPrint("[Matrix] Init matrix client");
      client = Client(widget.clientName, debug: false);
      onRoomKeyRequestSub ??=
          client.onRoomKeyRequest.stream.listen((RoomKeyRequest request) async {
        final Room room = request.room;
        final User sender = room.getUserByMXIDSync(request.sender);
        if (await SimpleDialogs(context).askConfirmation(
          titleText: I18n.of(context).requestToReadOlderMessages,
          contentText:
              "${sender.id}\n\n${I18n.of(context).device}:\n${request.requestingDevice.deviceId}\n\n${I18n.of(context).identity}:\n${request.requestingDevice.curve25519Key.beautified}",
          confirmText: I18n.of(context).verify,
          cancelText: I18n.of(context).deny,
        )) {
          await request.forwardKey();
        }
      });
      _initWithStore();
    } else {
      client = widget.client;
    }
    super.initState();
  }

  @override
  void dispose() {
    onRoomKeyRequestSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _InheritedMatrix(
      data: this,
      child: widget.child,
    );
  }
}

class _InheritedMatrix extends InheritedWidget {
  final MatrixState data;

  _InheritedMatrix({Key key, this.data, Widget child})
      : super(key: key, child: child);

  @override
  bool updateShouldNotify(_InheritedMatrix old) {
    bool update = old.data.client.accessToken != this.data.client.accessToken ||
        old.data.client.userID != this.data.client.userID ||
        old.data.client.matrixVersions != this.data.client.matrixVersions ||
        old.data.client.lazyLoadMembers != this.data.client.lazyLoadMembers ||
        old.data.client.deviceID != this.data.client.deviceID ||
        old.data.client.deviceName != this.data.client.deviceName ||
        old.data.client.homeserver != this.data.client.homeserver;
    return update;
  }
}
