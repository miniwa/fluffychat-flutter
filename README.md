# fluffychat-miniwa

Chat with your friends, miniwatrix edition.

[Download latest apk](https://www.youtube.com/watch?v=oavMtUWDBTM)

## How to build

1. [Install flutter](https://flutter.dev)

2. Clone the repo:
```
git clone --recurse-submodules https://gitlab.com/ChristianPauly/fluffychat-flutter
cd fluffychat-flutter
```

### Android

3. For Android install CMake from the SDK Manager

4. Install ninja:
```
sudo apt install ninja-build
```

5. Outcomment the Google Services plugin at the end of the file `android/app/build.gradle`:
```
// apply plugin: "com.google.gms.google-services"
```

6. `flutter run`

### Web

3. `flutter channel beta && flutter upgrade`

4. `flutter config --enable-web`

5. `flutter run`

## How to add translations for your language

don't
